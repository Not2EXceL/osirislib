/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.not2excel.osiris.lib.reflection;

import com.not2excel.osiris.lib.annotations.reflection.Obfuscated;
import com.not2excel.osiris.lib.exceptions.ReflectionException;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.Arrays;

public class ReflectionUtil
{
    /**
     * Returns array of {@link java.lang.reflect.Field} in <code>parent</code> with the specific <code>type</code>
     *
     * @param type
     *         class to filter {@link java.lang.reflect.Field} stream by
     * @param parent
     *         parent class to search fields
     * @return filtered {@link java.lang.reflect.Field} array
     */
    public static Field[] getFieldsByType(Class<?> type, Class<?> parent)
    {
        return Arrays.stream(parent.getDeclaredFields())
                               .filter(f -> f.getType().equals(type))
                               .sequential().toArray(Field[]::new);
    }

    /**
     * Returns array of {@link java.lang.reflect.Field} in <code>parent</code> with the specific <code>type</code>
     * and <code>annotation</code>
     *
     * @param type
     *         class to filter {@link java.lang.reflect.Field} stream by
     * @param parent
     *         parent class to search fields
     * @param annotation
     *         filters out fields without specified annotation
     * @return filtered {@link java.lang.reflect.Field} array
     */
    public static Field[] getFieldsByTypeWithAnnotation(Class<?> type, Class<?> parent,
                                                        Class<? extends Annotation> annotation)
    {
        return Arrays.stream(parent.getDeclaredFields())
                               .filter(f -> f.getType().equals(type) &&
                                            f.isAnnotationPresent(annotation))
                               .sequential().toArray(Field[]::new);
    }

    /**
     * Returns array of {@link java.lang.reflect.Field} in <code>parent</code> with the specific <code>annotation</code>
     *
     * @param parent
     *         parent class to search fields
     * @param annotation
     *         filters out fields without specified annotation
     * @return filtered {@link java.lang.reflect.Field} array
     */
    public static Field[] getFieldsWithAnnotation(Class<?> parent, Class<? extends Annotation> annotation)
    {
        return Arrays.stream(parent.getDeclaredFields())
                               .filter(f -> f.isAnnotationPresent(annotation))
                               .sequential().toArray(Field[]::new);
    }

    /**
     * Returns array of {@link java.lang.reflect.Method} in <code>parent</code> with the specific
     * <code>returnType</code>
     *
     * @param returnType
     *         type to check if {@link java.lang.reflect.Method#getReturnType()} isAssignableFrom
     * @param parent
     *         parent class to search methods
     * @return filtered {@link java.lang.reflect.Method} array
     */
    public static Method[] getMethodsByReturnType(Class<?> returnType, Class<?> parent)
    {
        return Arrays.stream(parent.getDeclaredMethods())
                                .filter(m -> m.getReturnType().isAssignableFrom(returnType))
                                .sequential().toArray(Method[]::new);
    }

    /**
     * Returns array of {@link java.lang.reflect.Method} in <code>parent</code> with the specific
     * <code>returnType</code> and <code>annotation</code>
     *
     * @param returnType
     *         type to check if {@link java.lang.reflect.Method#getReturnType()} isAssignableFrom
     * @param parent
     *         parent class to search methods
     * @param annotation
     *         filters out methods without specified annotation
     * @return filtered {@link java.lang.reflect.Method} array
     */
    public static Method[] getMethodsByReturnTypeWithAnnotation(Class<?> returnType, Class<?> parent,
                                                                Class<? extends Annotation> annotation)
    {
        return Arrays.stream(parent.getDeclaredMethods())
                                .filter(m -> m.getReturnType().isAssignableFrom(returnType) &&
                                             m.isAnnotationPresent(annotation))
                                .sequential().toArray(Method[]::new);
    }

    /**
     * Returns array of {@link java.lang.reflect.Method} in <code>parent</code> with the specific
     * <code>parameterTypes</code>
     *
     * @param parameterTypes
     *         parameter types to check if {@link java.lang.reflect.Method#getParameterTypes()}  is equivalent to
     * @param parent
     *         parent class to search methods
     * @return filtered {@link java.lang.reflect.Method} array
     */
    public static Method[] getMethodsByParameterTypes(Class<?>[] parameterTypes, Class<?> parent)
    {
        return Arrays.stream(parent.getDeclaredMethods())
                                .filter(m -> Arrays.deepEquals(parameterTypes, m.getParameterTypes()))
                                .sequential().toArray(Method[]::new);
    }

    /**
     * Returns array of {@link java.lang.reflect.Method} in <code>parent</code> with the specific
     * <code>parameterTypes</code> and <code>annotation</code>
     *
     * @param parameterTypes
     *         parameter types to check if {@link java.lang.reflect.Method#getParameterTypes()}  is equivalent to
     * @param parent
     *         parent class to search methods
     * @param annotation
     *         filters out methods without specified annotation
     * @return filtered {@link java.lang.reflect.Method} array
     */
    public static Method[] getMethodsByParameterTypesWithAnnotation(Class<?>[] parameterTypes, Class<?> parent,
                                                                    Class<? extends Annotation> annotation)
    {
        return Arrays.stream(parent.getDeclaredMethods())
                                .filter(m -> Arrays.deepEquals(parameterTypes, m.getParameterTypes()) &&
                                             m.isAnnotationPresent(annotation))
                                .sequential().toArray(Method[]::new);
    }

    /**
     * Returns array of {@link java.lang.reflect.Method} in <code>parent</code> with the specific
     * <code>annotation</code>
     *
     * @param parent
     *         parent class to search methods
     * @param annotation
     *         filters out methods without specified annotation
     * @return filtered {@link java.lang.reflect.Method} array
     */
    public static Method[] getMethodsWithAnnotation(Class<?> parent, Class<? extends Annotation> annotation)
    {
        return Arrays.stream(parent.getDeclaredMethods())
                                .filter(m -> m.isAnnotationPresent(annotation))
                                .sequential().toArray(Method[]::new);
    }

    /**
     * Non-static {@link Object} retrieval
     * Also has ability to retrieve static Field objects
     *
     * @param clazz
     *         class to filter {@link java.lang.reflect.Field} stream by
     * @param parent
     *         parent object to search fields and retrieve value
     * @param index
     *         index of Field in array
     * @param <T>
     *         return type
     * @return T
     */
    public static <T> T getFieldValue(Class<T> clazz, Object parent, int index)
    {
        Field[] fields = getFieldsByType(clazz, parent.getClass());
        if (fields.length <= index)
        {
            throw new ReflectionException(String.format("Index %d is too high.", index));
        }
        if (!fields[index].isAccessible())
        {
            fields[index].setAccessible(true);
        }
        try
        {
            return clazz.cast(fields[index].get(Modifier.isStatic(fields[index].getModifiers()) ? null : parent));
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Static {@link Object} retrieval
     *
     * @param clazz
     *         class to filter Field stream by
     * @param parent
     *         parent class to search fields
     * @param index
     *         index of Field in array
     * @param <T>
     *         return type
     * @return T
     */
    public static <T> T getFieldValue(Class<T> clazz, Class<?> parent, int index)
    {
        Field[] fields = getFieldsByType(clazz, parent.getClass());
        if (fields.length <= index)
        {
            throw new ReflectionException(String.format("Index %d is too high.", index));
        }
        if (!Modifier.isStatic(fields[index].getModifiers()))
        {
            throw new ReflectionException("Cannot retrieve value as it is not static.");
        }
        if (!fields[index].isAccessible())
        {
            fields[index].setAccessible(true);
        }
        try
        {
            return clazz.cast(fields[index].get(null));
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Sets a {@link java.lang.reflect.Field} with a new value
     * static or not is irrelevant
     *
     * @param value
     *         value to set field object with
     * @param parent
     *         parent object to search fields and set value if not static
     * @param index
     *         index of Field in array
     */
    public static void setFieldValue(Object value, Object parent, int index)
    {
        Field[] fields = getFieldsByType(value.getClass(), parent.getClass());
        if (fields.length <= index)
        {
            throw new ReflectionException(String.format("Index %d is too high.", index));
        }
        if (Modifier.isFinal(fields[index].getModifiers()))
        {
            throw new ReflectionException("Field is final.");
        }
        try
        {
            if (!fields[index].isAccessible())
            {
                fields[index].setAccessible(true);
            }
            fields[index].set(Modifier.isStatic(fields[index].getModifiers()) ? null : parent, value);
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Sets a {@link java.lang.reflect.Field} with a new value
     * solely static fields
     *
     * @param value
     *         value to set field object with
     * @param parent
     *         parent class to search fields and set value
     * @param index
     *         index of Field in array
     */
    public static void setFieldValue(Object value, Class<?> parent, int index)
    {
        Field[] fields = getFieldsByType(value.getClass(), parent);
        if (fields.length <= index)
        {
            throw new ReflectionException(String.format("Index %d is too high.", index));
        }
        if (Modifier.isFinal(fields[index].getModifiers()) ||
            Modifier.isStatic(fields[index].getModifiers()))
        {
            throw new ReflectionException(String.format("Field is final:%s and/or static:%s.",
                                                        Modifier.isFinal(fields[index].getModifiers()),
                                                        Modifier.isStatic(fields[index].getModifiers())));
        }
        try
        {
            if (!fields[index].isAccessible())
            {
                fields[index].setAccessible(true);
            }
            fields[index].set(null, value);
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Non-static {@link java.lang.reflect.Field} value setting
     *
     * @param field
     *         Field to set value of
     * @param value
     *         value to set field object with
     * @param parent
     *         parent object to search fields and set value if not static
     */
    public static void setFieldValue(Field field, Object value, Object parent)
    {
        if (!field.getType().isAssignableFrom(value.getClass()))
        {
            throw new ReflectionException(String.format("%s is not assignable from %s", field.getType().getName(),
                                                        value.getClass().getName()));
        }
        if (Modifier.isFinal(field.getModifiers()))
        {
            throw new ReflectionException("Field is final.");
        }
        try
        {
            if (!field.isAccessible())
            {
                field.setAccessible(true);
            }
            field.set(Modifier.isStatic(field.getModifiers()) ? null : parent, value);
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Static {@link java.lang.reflect.Field} value setting
     *
     * @param field
     *         Field to set value of
     * @param value
     *         value to set field object with
     * @param parent
     *         parent class to search fields
     */
    public static void setFieldValue(Field field, Object value, Class<?> parent)
    {
        if (!field.getType().isAssignableFrom(value.getClass()))
        {
            throw new ReflectionException(String.format("%s is not assignable from %s", field.getType().getName(),
                                                        value.getClass().getName()));
        }
        if (Modifier.isFinal(field.getModifiers()) ||
            Modifier.isStatic(field.getModifiers()))
        {
            throw new ReflectionException(String.format("Field is final:%s and/or static:%s.",
                                                        Modifier.isFinal(field.getModifiers()),
                                                        Modifier.isStatic(field.getModifiers())));
        }
        try
        {
            if (!field.isAccessible())
            {
                field.setAccessible(true);
            }
            field.set(null, value);
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Returns {@link java.lang.reflect.Field} with unobfuscated name in
     * {@link com.not2excel.osiris.lib.annotations.reflection.Obfuscated} annotation
     *
     * @param name
     *         unobfuscated name
     * @param parent
     *         parent class to search fields
     * @return Field if found, else null
     */
    public static Field getObfuscatedField(String name, Class<?> parent)
    {
        Field[] fields = getFieldsWithAnnotation(parent, Obfuscated.class);
        for (Field f : fields)
        {
            if (f.getAnnotation(Obfuscated.class).name().equalsIgnoreCase(name))
            {
                return f;
            }
        }
        return null;
    }

    /**
     * Non-static ObfuscatedField setting
     * static setting is tolerated
     *
     * @param name
     *         unobfuscated name
     * @param value
     *         value to set field object with
     * @param parent
     *         parent object to search fields and set value if not static
     */
    public static void setObfuscatedFieldValue(String name, Object value, Object parent)
    {
        Field f = getObfuscatedField(name, parent.getClass());
        if (f != null)
        {
            setFieldValue(f, value, parent);
        }
    }

    /**
     * Static ObfuscatedField setting
     *
     * @param name
     *         unobfuscated name
     * @param value
     *         value to set field object with
     * @param parent
     *         parent class to search fields
     */
    public static void setObfuscatedFieldValue(String name, Object value, Class<?> parent)
    {
        Field f = getObfuscatedField(name, parent);
        if (f != null)
        {
            setFieldValue(f, value, parent);
        }
    }

    /**
     * Returns {@link java.lang.reflect.Method} with unobfuscated name in
     * {@link com.not2excel.osiris.lib.annotations.reflection.Obfuscated} annotation
     *
     * @param name
     *         unobfuscated name
     * @param parent
     *         parent class to search methods
     * @return Method if found, else null
     */
    public static Method getObfuscatedMethod(String name, Class<?> parent)
    {
        Method[] methods = getMethodsWithAnnotation(parent, Obfuscated.class);
        for (Method m : methods)
        {
            if (m.getAnnotation(Obfuscated.class).name().equalsIgnoreCase(name))
            {
                return m;
            }
        }
        return null;
    }

    /**
     * Instantiates a new class object, too lazy to wrap all of them
     *
     * @param clazz
     *         Class to instantiate
     * @param <T>
     *         type
     * @return instantiated object
     */
    public static <T> T instantiate(Class<T> clazz)
    {
        try
        {
            Constructor<T> constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            return constructor.newInstance();
        }
        catch (NoSuchMethodException | InvocationTargetException |
                InstantiationException | IllegalAccessException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Attempts to return the class based on the fullname
     *
     * @param name
     *         fullname of the class
     * @return class found else null
     */
    public static Class<?> findClass(String name)
    {
        try
        {
            return Class.forName(name);
        }
        catch (ClassNotFoundException | ExceptionInInitializerError e)
        {
            System.out.println(name);
            e.printStackTrace();
            return null;
        }
    }
}
