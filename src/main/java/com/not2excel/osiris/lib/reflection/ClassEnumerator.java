/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.not2excel.osiris.lib.reflection;

import com.not2excel.osiris.lib.exceptions.ClassEnumeratorException;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

public class ClassEnumerator
{
    /**
     * Filters a list of classes by annotation
     *
     * @param input
     *         class list found by ClassEnumerator
     * @param annotation
     *         annotation to filter by
     * @return filtered list
     */
    public static List<Class<?>> filterByAnnotation(final List<Class<?>> input, Class<? extends Annotation> annotation)
    {
        return input.stream().filter(c -> c.isAnnotationPresent(annotation)).collect(Collectors.toList());
    }

    /**
     * Personaly usage mainly, cuz lazy
     *
     * @param clazz
     *         clazz for package
     * @param annotation
     *         annotation to filter by
     * @return filtered Class list
     */
    public static List<Class<?>> getAndFilterByAnnotation(final Class<?> clazz,
                                                          final Class<? extends Annotation> annotation)
    {
        return filterByAnnotation(getClassesFromPackage(clazz), annotation);
    }

    /**
     * Parses a directory for jar files and class files
     * <p/>
     * Recurses through if necessary
     *
     * @param directory
     *         directory to parse
     * @return class array
     */
    public static List<Class<?>> getClassesFromExternalDirectory(final File directory)
    {
        final List<Class<?>> classes = new ArrayList<>();
        File[] files = directory.listFiles();
        if (files == null)
        {
            throw new ClassEnumeratorException("Can't load classes from an empty/null directory file list.");
        }
        for (final File file : files)
        {
            try
            {
                final ClassLoader classLoader = new URLClassLoader(new URL[]{
                        file.toURI().toURL()
                }, ClassEnumerator.class.getClassLoader());
                if (file.getName().toLowerCase().trim().endsWith(".class"))
                {
                    classes.add(classLoader.loadClass(file.getName().replace(".class", "")
                                                          .replace("/", ".")));
                }
                else if (file.getName().toLowerCase().trim().endsWith(".jar"))
                {
                    classes.addAll(getClassesFromJar(file, classLoader));
                }
                else if (file.isDirectory())
                {
                    classes.addAll(getClassesFromExternalDirectory(file));
                }
            }
            catch (final MalformedURLException | ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }
        return classes;
    }

    /**
     * Returns the class array of all classes within a package
     *
     * @param clazz
     *         class to get code source location for
     * @return class list
     */
    public static List<Class<?>> getClassesFromPackage(final Class<?> clazz)
    {
        final List<Class<?>> classes = new ArrayList<>();
        String pkgName = clazz.getPackage().getName().replace(".", "/");
        File path = null;
        try
        {
            path = new File(URLDecoder.decode(clazz.getProtectionDomain().getCodeSource().getLocation().getPath() +
                                              pkgName, "UTF-8"));
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        if (path != null)
        {
            classes.addAll(processDirectory(path, clazz.getPackage().getName()));
        }
        return classes;
    }

    /**
     * Returns all class files inside a jar
     *
     * @param file
     *         jar file
     * @param classLoader
     *         classloader created previously using the jar file
     * @return class list
     */
    public static List<Class<?>> getClassesFromJar(final File file, final ClassLoader classLoader)
    {
        final List<Class<?>> classes = new ArrayList<>();
        try
        {
            final JarFile jarFile = new JarFile(file);
            final Enumeration<JarEntry> enumeration = jarFile.entries();
            while (enumeration.hasMoreElements())
            {
                final JarEntry jarEntry = enumeration.nextElement();
                if (jarEntry.isDirectory() || !jarEntry.getName().toLowerCase().trim().endsWith(".class"))
                {
                    continue;
                }
                classes.add(classLoader.loadClass(jarEntry.getName().replace(".class", "")
                                                          .replace("/", ".")));
            }
            jarFile.close();
        }
        catch (final IOException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        return classes;
    }

    /**
     * Processes a directory and retrieves all classes from it and its
     * subdirectories
     * Recurses if necessary
     *
     * @param directory
     *         directory file to traverse
     * @return list of classes
     */
    private static List<Class<?>> processDirectory(final File directory, final String append)
    {
        final List<Class<?>> classes = new ArrayList<>();
        for (final String fileName : directory.list())
        {
            if (fileName.endsWith(".class"))
            {
                String cName = String.format("%s.%s", append,
                                             fileName.replace(".class", "").replace("/", "."));
                if (cName.startsWith("/"))
                {
                    cName = cName.substring(1);
                }
                classes.add(ReflectionUtil.findClass(cName));
            }
            else
            {
                final File subDir = new File(directory, fileName);
                if (subDir.isDirectory())
                {
                    classes.addAll(processDirectory(subDir, append + "." + fileName));
                }
            }
        }
        return classes;
    }
}
