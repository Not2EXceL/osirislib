/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.not2excel.osiris.lib.metadata;

import com.not2excel.osiris.lib.interfaces.metadata.IMetadata;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public abstract class MetadataTable<K, V>
{
    @Getter
    private       Object               parent;
    @Getter
    private final Map<K, IMetadata<V>> metadataTable;

    public MetadataTable(Object parent)
    {
        this.parent = parent;
        metadataTable = new HashMap<>();
    }

    public void insertKeyValue(K key, IMetadata<V> value)
    {
        synchronized (metadataTable)
        {
            if (!metadataTable.containsKey(key))
            {
                metadataTable.put(key, value);
            }
            else
            {
                throw new IllegalArgumentException("That key already exists.");
            }
        }
    }

    public void editKeyValue(K key, IMetadata<V> newValue)
    {
        synchronized (metadataTable)
        {
            if (metadataTable.containsKey(key))
            {
                metadataTable.remove(key);
                metadataTable.put(key, newValue);
            }
            else
            {
                throw new IllegalArgumentException("That key doesn't exist.");
            }
        }
    }

    public void removeKey(K key)
    {
        synchronized (metadataTable)
        {
            if (metadataTable.containsKey(key))
            {
                metadataTable.remove(key);
            }
            else
            {
                throw new IllegalArgumentException("That key doesn't exist.");
            }
        }
    }

    public IMetadata<V> getKeyValue(K key)
    {
        synchronized (metadataTable)
        {
            if (metadataTable.containsKey(key))
            {
                return metadataTable.get(key);
            }
            throw new IllegalArgumentException("That key doesn't exist.");
        }
    }

    public void invalidate()
    {
        parent = null;
        synchronized (metadataTable)
        {
            metadataTable.clear();
        }
    }
}
