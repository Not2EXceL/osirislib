package com.not2excel.osiris.lib.datastructures;

/**
 * @author Richmond Steele
 * @since 5/8/2014
 * All rights Reserved
 * Please read included LICENSE file
 */
public class Vec3<K, V, T>
{
    private K key;
    private V value;
    private T type;

    public Vec3(K key, V value, T type)
    {
        this.key = key;
        this.value = value;
        this.type = type;
    }

    public K getKey()
    {
        return key;
    }

    public void setKey(K key)
    {
        this.key = key;
    }

    public V getValue()
    {
        return value;
    }

    public void setValue(V value)
    {
        this.value = value;
    }

    public T getType()
    {
        return type;
    }

    public void setType(T type)
    {
        this.type = type;
    }
}
