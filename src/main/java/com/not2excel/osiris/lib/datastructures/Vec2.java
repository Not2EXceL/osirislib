package com.not2excel.osiris.lib.datastructures;

/**
 * @author Richmond Steele
 * @since 5/8/2014
 * All rights Reserved
 * Please read included LICENSE file
 */
public class Vec2<K, V>
{
    private K key;
    private V value;

    public Vec2(K key, V value)
    {
        this.key = key;
        this.value = value;
    }

    public K getKey()
    {
        return key;
    }

    public void setKey(K key)
    {
        this.key = key;
    }

    public V getValue()
    {
        return value;
    }

    public void setValue(V value)
    {
        this.value = value;
    }
}
