/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.not2excel.osiris.lib.datastructures;

import com.not2excel.osiris.lib.exceptions.TahgException;
import lombok.Getter;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

public class TahgManager<T>
{
    @Getter
    protected final Map<String, T> tahgMap = new ConcurrentHashMap<>();

    public void addTahg(String label, T tahg)
    {
        if (tahgMap.containsKey(label.toLowerCase()))
        {
            tahgMap.remove(label.toLowerCase());
        }
        tahgMap.put(label.toLowerCase(), tahg);
    }

    public void removeTahg(String label)
    {
        if (tahgMap.containsKey(label.toLowerCase()))
        {
            tahgMap.remove(label.toLowerCase());
        }
        else
        {
            throw new TahgException(label.concat(" tahg doesn't exist."));
        }
    }

    public void removeTahg(T tahg)
    {
        if (tahgMap.containsValue(tahg))
        {
            Iterator<Entry<String, T>> iterator = tahgMap.entrySet().iterator();
            while (iterator.hasNext())
            {
                if (iterator.next().equals(tahg))
                {
                    iterator.remove();
                    break;
                }
            }
        }
        else
        {
            throw new TahgException(tahg + " isn't a value.");
        }
    }

    public void editTahg(String label, T tahg)
    {
        if (tahgMap.containsKey(label.toLowerCase()))
        {
            tahgMap.remove(label.toLowerCase());
            tahgMap.put(label.toLowerCase(), tahg);
        }
        else
        {
            throw new TahgException(label.concat(" doesn't exist."));
        }
    }
}
