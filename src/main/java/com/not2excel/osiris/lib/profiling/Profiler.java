/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.not2excel.osiris.lib.profiling;

import com.not2excel.osiris.lib.logging.PrefixedLogger;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Profiler
{
    private final Map<String, Long> profiles = new ConcurrentHashMap<>();
    private       PrefixedLogger    logger   = new PrefixedLogger("Profiler");
    private String currentProfile;

    public void startProfile(String profile)
    {
        currentProfile = profile.toLowerCase();
        profiles.put(profile.toLowerCase(), nanoTime());
    }

    public void endProfile()
    {
        endProfile(currentProfile);
    }

    public void endProfile(String profile)
    {
        if (profiles.containsKey(profile.toLowerCase()))
        {
            logProfile(profile.toLowerCase(), true);
            profiles.remove(profile);
        }
    }

    private void logProfile(String profile, boolean end)
    {
        if (end)
        {
            logger.log(String.format("Ended profile: %s. Took %s.", profile,
                                     getTimeDiff(profiles.get(profile))));
        }
        else
        {
            logger.log(String.format("Started profile: %s.", profile));
        }
    }

    private String getTimeDiff(long start)
    {
        double millisDiff = getMillisTimeDiff(start);
        if (millisDiff > 1000)
        {
            return (millisDiff / 1000D) + " seconds";
        }
        return millisDiff + " milliseconds";
    }

    private long getMillisTimeDiff(long start)
    {
        return (nanoTime() - start) / 1000000;
    }

    public long nanoTime()
    {
        return System.nanoTime();
    }
}
