/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.not2excel.osiris.lib.event;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.not2excel.osiris.lib.annotations.event.EventListener;
import com.not2excel.osiris.lib.event.events.EventBase;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EventManager
{
    INSTANCE;

    @Getter
    private static final Map<Class<? extends EventBase>, EventDispatcher> dispatchers = new ConcurrentHashMap<>();
    @Getter
    private final        EventFilterCache                                 filterCache = new EventFilterCache();

    public static <E extends EventBase> E dispatch(E e)
    {
        if (!dispatchers.containsKey(e.getClass()))
        {
            return e;
        }
        dispatchers.get(e.getClass()).dispatch(e);
        return e;
    }

    public void subscribe(final Object parent)
    {
        ListMultimap<Class<? extends EventBase>, RegisteredListener> listeners = listenerMapFromParent(parent);
        for (Class<? extends EventBase> eventClass : listeners.keySet())
        {
            if (!dispatchers.containsKey(eventClass))
            {
                dispatchers.put(eventClass, new EventDispatcher());
            }
            dispatchers.get(eventClass).subscribe(listeners.get(eventClass));
        }
    }

    public void unsubscribe(final Object parent)
    {
        ListMultimap<Class<? extends EventBase>, RegisteredListener> listeners = listenerMapFromParent(parent);
        for (Class<? extends EventBase> eventClass : listeners.keySet())
        {
            if (!dispatchers.containsKey(eventClass))
            {
                dispatchers.put(eventClass, new EventDispatcher());
                continue;
            }
            dispatchers.get(eventClass).unsubscribe(listeners.get(eventClass));
        }
    }

    private ListMultimap<Class<? extends EventBase>, RegisteredListener> listenerMapFromParent(final Object parent)
    {
        ListMultimap<Class<? extends EventBase>, RegisteredListener> multimap = ArrayListMultimap.create();
        Arrays.stream(parent.getClass().getMethods())
              .parallel()
              .filter(m -> m.isAnnotationPresent(EventListener.class))
              .forEach(m -> multimap.put(m.getAnnotation(EventListener.class).event(),
                                         new RegisteredListener(parent, m)));
        return multimap;
    }
}
