/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.not2excel.osiris.lib.event;

import com.not2excel.osiris.lib.annotations.event.EventListener;
import com.not2excel.osiris.lib.event.events.EventBase;
import com.not2excel.osiris.lib.interfaces.filter.IEventFilter;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

@Value
public class RegisteredListener
{
    private Map<Class<? extends Annotation>, Annotation> annotations = new Hashtable<>();
    @NonFinal
    private Object parent;
    private Method             method;
    private EventListener      listener;
    private boolean            ignoreCancelled;
    private int                priority;
    private int                argsLength;
    private List<IEventFilter> filters;
    @NonFinal
    private boolean reset = false;

    public RegisteredListener(final Object parent, final Method method)
    {
        if (parent == null || method == null)
        {
            throw new IllegalArgumentException();
        }
        this.parent = parent;
        this.method = method;
        if (Modifier.isStatic(method.getModifiers()))
        {
            this.parent = null;
        }
        if (!method.isAccessible())
        {
            method.setAccessible(true);
            reset = true;
        }
        this.argsLength = method.getParameterCount() >= 1 ? 1 : 0;
        Arrays.stream(method.getAnnotations()).parallel().forEach(a -> annotations.put(a.annotationType(), a));
        this.listener = getAnnotation(EventListener.class);
        this.ignoreCancelled = this.listener.ignoreCancelled();
        this.filters = new ArrayList<>();
        if (this.listener.filters() != null && this.listener.filters().length > 0)
        {
            for (Class<? extends IEventFilter> filter : this.listener.filters())
            {
                filters.add(EventManager.INSTANCE.getFilterCache().getFiltersByClass(filter));
            }
        }
        this.priority = listener.priority();
    }

    public void fire(EventBase event)
    {
        try
        {
            if (argsLength == 1)
            {
                this.method.invoke(parent, event);
            }
            else
            {
                this.method.invoke(parent);

            }
        }
        catch (IllegalAccessException | InvocationTargetException e)
        {
            e.printStackTrace();
        }
    }

    public <A extends Annotation> A getAnnotation(Class<A> a)
    {
        return a.cast(annotations.get(a));
    }
}
