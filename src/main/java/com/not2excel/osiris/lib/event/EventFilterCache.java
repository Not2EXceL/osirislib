/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.not2excel.osiris.lib.event;

import com.not2excel.osiris.lib.interfaces.filter.IEventFilter;
import com.not2excel.osiris.lib.interfaces.filter.IFilterCache;
import lombok.Getter;

import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class EventFilterCache implements IFilterCache<IEventFilter>
{
    @Getter
    private Map<Class<? extends IEventFilter>, IEventFilter> filters = new ConcurrentHashMap<>();

    @Override
    public IEventFilter getFiltersByClass(Class<? extends IEventFilter> clazz)
    {
        if (IEventFilter.class.isAssignableFrom(clazz))
        {
            Class<? extends IEventFilter> eventFilterClass = clazz.asSubclass(IEventFilter.class);
            if (filters.containsKey(eventFilterClass))
            {
                return filters.get(eventFilterClass);
            }
            else
            {
                instantiateFilter(eventFilterClass);
                return filters.get(eventFilterClass);
            }
        }
        throw new IllegalArgumentException("IEventFilter isn't assignable from clazz");
    }

    private void instantiateFilter(Class<? extends IEventFilter> filter)
    {
        if (!filter.isMemberClass() || Modifier.isStatic(filter.getModifiers()))
        {
            try
            {
                filters.put(filter, filter.newInstance());
            }
            catch (InstantiationException | IllegalAccessException e)
            {
                e.printStackTrace();
            }
        }
    }
}
