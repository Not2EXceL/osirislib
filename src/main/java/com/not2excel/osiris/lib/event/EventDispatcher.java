/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.not2excel.osiris.lib.event;

import com.not2excel.osiris.lib.event.events.EventBase;
import com.not2excel.osiris.lib.event.events.StoppableEvent;
import com.not2excel.osiris.lib.interfaces.filter.IEventFilter;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class EventDispatcher
{
    @Getter
    private final List<RegisteredListener> listeners = Collections.synchronizedList(new ArrayList<>());

    public <E extends EventBase> void dispatch(E e)
    {
        synchronized (listeners)
        {
            StoppableEvent stoppable = null;
            if(e instanceof StoppableEvent)
            {
                stoppable = StoppableEvent.class.cast(e);
            }
            for(RegisteredListener listener : listeners)
            {
                if(stoppable != null && stoppable.isStopped())
                {
                    break;
                }
                for(IEventFilter filter : listener.getFilters())
                {
                    if(!filter.passed(e, listener))
                    {
                        return;
                    }
                }
                if(!listener.isIgnoreCancelled())
                {
                    listener.fire(e);
                }
            }
        }
    }

    public void subscribe(final List<RegisteredListener> pListeners)
    {
        if (pListeners == null || pListeners.size() == 0)
        {
            return;
        }
        listeners.addAll(pListeners);
        sortListeners();
    }

    public void unsubscribe(final List<RegisteredListener> pListeners)
    {
        if (pListeners == null || pListeners.size() == 0)
        {
            return;
        }
        synchronized (listeners)
        {
            Iterator<RegisteredListener> iterator = listeners.iterator();
            while (iterator.hasNext())
            {
                RegisteredListener listener = iterator.next();
                if (pListeners.contains(listener))
                {
                    if(listener.isReset())
                    {
                        listener.getMethod().setAccessible(false);
                    }
                    iterator.remove();
                }
            }
        }
        sortListeners();
    }

    private void sortListeners()
    {
        synchronized (listeners)
        {
            listeners.sort((l1, l2) -> l1.getPriority() - l2.getPriority());
        }
    }


}
