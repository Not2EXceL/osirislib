/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.not2excel.osiris.lib.plugin;

import com.not2excel.osiris.lib.datastructures.TahgManager;

public class PluginManager extends TahgManager<AbstractPlugin>
{
    public AbstractPlugin findPluginByClass(Class<? extends AbstractPlugin> clazz)
    {
        for(AbstractPlugin plugin : tahgMap.values())
        {
            if(plugin.getClass().equals(clazz))
            {
                return plugin;
            }
        }
        return null;
    }

    public boolean isPluginToggled(Class<? extends AbstractPlugin> clazz)
    {
        AbstractPlugin plugin = findPluginByClass(clazz);
        return plugin != null && plugin.isToggled();
    }
}

