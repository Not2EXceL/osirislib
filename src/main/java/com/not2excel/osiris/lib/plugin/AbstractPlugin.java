/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.not2excel.osiris.lib.plugin;

import com.not2excel.osiris.lib.annotations.loading.Loadable;
import com.not2excel.osiris.lib.event.EventManager;
import com.not2excel.osiris.lib.interfaces.loading.IInitializable;
import com.not2excel.osiris.lib.interfaces.keybind.IKeybindable;
import com.not2excel.osiris.lib.interfaces.module.IPlugin;
import com.not2excel.osiris.lib.interfaces.module.IToggleable;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Loadable
public abstract class AbstractPlugin implements IPlugin, IKeybindable, IToggleable, IInitializable
{
    private final List<Integer> keybinds = new ArrayList<>();
    @Getter
    private       boolean       toggled  = false;

    @Override
    public void addKeybind(int... keybind)
    {
        for (int key : keybind)
        {
            if (!keybinds.contains(key))
            {
                keybinds.add(key);
            }
        }
    }

    @Override
    public void removeKeybind(int keybind)
    {
        if (keybinds.contains(keybind))
        {
            keybinds.remove(keybind);
        }
    }

    @Override
    public void removeKeybinds()
    {
        keybinds.clear();
    }

    @Override
    public boolean hasKeybind(int keybind)
    {
        return keybinds.contains(keybind);
    }

    @Override
    public Integer[] getKeybinds()
    {
        return keybinds.toArray(new Integer[keybinds.size()]);
    }

    @Override
    public void toggle()
    {
        if (toggled)
        {
            enable();
        }
        else
        {
            disable();
        }
    }

    @Override
    public void enable()
    {
        toggled = true;
        EventManager.INSTANCE.subscribe(this);
        onEnable();
    }

    protected void onEnable(){}

    @Override
    public void disable()
    {
        toggled = false;
        EventManager.INSTANCE.unsubscribe(this);
        onDisable();
    }

    protected void onDisable(){}
}
